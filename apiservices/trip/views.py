from typing import Any
from django.db.models.base import Model as Model
from account.models import User
from django.views.generic import ListView,DetailView
from django.shortcuts import render,get_object_or_404
from .models import Flight,Hadis,Category


# Create your views here.
# def home(request,page=1):
#     flight_list = Flight.objects.published()
#     paginator = Paginator(flight_list, 4)
#     flights = paginator.get_page(page)
#     context={
#         'flights' : flights,
#         'hadises' : Hadis.objects.all()[:1],
#         'category': Category.objects.filter(status = True),
#     }
#     return render(request,'trip/home.html',context)


# def detail(request,slug):
#     context={
#         # 'flight' : Flight.objects.get(slug=slug)
#         'flight' : get_object_or_404(Flight.objects.published(),slug=slug)
        
#     }
  
#     return render(request,'trip/detail.html',context)


class HadisListView(ListView):
     model = Hadis
     
class FlightListView(ListView): 
    queryset = Flight.objects.published()
    paginate_by = 4

class FlightDetailview(DetailView):
    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Flight.objects.published(),slug=slug)

class CategoryList(ListView):
    paginate_by = 4
    template_name= 'trip/category_list.html'
    def get_queryset(self): 
        global category
        slug = self.kwargs.get('slug')
        category = get_object_or_404(Category.objects.active(),slug = slug)
        return category.trips.published()
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context["category"] = category
        return context

class AuthorList(ListView):
       paginate_by = 4
       template_name= 'trip/author_list.html'
       def get_queryset(self): 
            global author
            useername = self.kwargs.get('useername')
            author = get_object_or_404(User,slug = useername)
            return author.trips.published()
    
       def get_context_data(self, **kwargs):
            # Call the base implementation first to get a context
            context = super().get_context_data(**kwargs)
            # Add in a QuerySet of all the books
            context["author"] = author
            return context