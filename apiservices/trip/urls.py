from django.urls import path
from .views import CategoryList,FlightListView,FlightDetailview,AuthorList

app_name = "trip"
urlpatterns = [
    path('',FlightListView.as_view() ,name="home"),
    path('page/<int:page>',FlightListView.as_view() ,name="home"),
    path('trip/<slug:slug>',FlightDetailview.as_view(),name='detail'),
    path('category/<slug:slug>',CategoryList.as_view(),name='category'),
    path('author/<slug:username>',AuthorList.as_view(),name='author') 
]