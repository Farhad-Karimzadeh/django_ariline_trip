from django.db import models
from account.models import User
from django.urls import reverse
from django.utils.html import format_html
from django.utils import timezone
from extensions.utils import jalali_converter

#flight manager
class FlightManager(models.Manager):
    def published(self):
        return self.filter(status = "p")
    
#category manager
class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status = True)

#  data models .
class Category(models.Model):
    title= models.CharField(max_length = 200,verbose_name="عنوان دسته بندی")
    slug = models.SlugField(max_length = 200,verbose_name=" آدرس دسته بندی ها")
    status= models.BooleanField(default = True,verbose_name="وضعیت پرواز: ")
    position = models.IntegerField(verbose_name = "پوزیشن")

    class Meta:
       verbose_name = "دسته بندی "
       verbose_name_plural = "دسته بندی ها"
       ordering = ["position"]
    def __str__(self):
       return(self.title)
    objects = CategoryManager()

class Hadis(models.Model):
    sentence = models.TextField(verbose_name = "احادیث")
    created = models.DateTimeField(auto_now_add= True,verbose_name = "آخرین بروز رسانی")

    def __str__(self):
        return (self.sentence)
    class Meta:
        verbose_name = "حدیث"
        verbose_name_plural = "احادیث"
        ordering = ["-created"]
    def jcreated(self):
        return jalali_converter(self.created)
    jcreated.short_description= "آخرین بروزرسانی"
    
class Flight(models.Model):
    ft_choices = {
       'EC' : 'اکونومی',
       'BS' : 'بیزینس',
    }
    st_choices = {
        'DO' : 'داخلی',
        'IN' : 'خارجی',
    }
    STATUS_CHOISES={
        'p':'پرواز',
        'd':'کنسل'
    }
    flight_type = models.CharField(max_length = 2,choices = ft_choices,default = "اکونومی",verbose_name="نوع پرواز")
    company = models.CharField(max_length = 100,verbose_name="شرکت هواپیمایی")
    author = models.ForeignKey(User,on_delete=models.SET_NULL,null=True,verbose_name="نویسنده",related_name = "flights")
    slug = models.SlugField(max_length = 200, unique = True,verbose_name="شماره پرواز") #fly no
    category = models.ManyToManyField(Category,verbose_name="دسته بندی",related_name="trips")
    source = models.CharField(max_length = 100,verbose_name="مبدا")
    destination = models.CharField(max_length = 100,verbose_name="مقصد")
    date = models.DateTimeField(verbose_name="تاریخ پرواز")
    fly_time = models.TimeField(verbose_name="ساعت پرواز")
    landing_time = models.TimeField(verbose_name="ساعت فرود")
    price = models.IntegerField(verbose_name="قیمت")
    terminal = models.CharField(null=True,max_length = 100,verbose_name="ترمینال")
    rate_class = models.CharField(max_length = 100,null=True,verbose_name="کلاس پرواز")
    cargo = models.IntegerField(verbose_name="بار مجاز",null=True) #bar mojaz
    airplane_model = models.CharField(null=True,max_length = 100,verbose_name="مدل هواپیما")
    thumbnail = models.ImageField(upload_to ="images")
    service_type = models.CharField(max_length = 2, choices = st_choices,default = "داخلی" ,verbose_name="مسیر پرواز")
    updated = models.DateTimeField(auto_now_add= True,null=True)
    status = models.CharField(max_length = 1,choices = STATUS_CHOISES,default = "پرواز",verbose_name= "وضعیت")
    # hadis = models.ForeignKey("Hadis", verbose_name="حدیث", on_delete=models.CASCADE)

    def __str__(self):
        return(self.source + " ----> " +self.destination)
    class Meta:
        verbose_name = "پرواز "
        verbose_name_plural = "پروازها"
        ordering = ["-updated"]

    def jdate(self):
        return jalali_converter(self.date)
    jdate.short_description="تاریخ پرواز"
    
    def jupdated(self):
        return jalali_converter(self.updated)
    
    def get_absolute_url(self):
        return reverse("account:home")
    
    
    def category_published(self):
        return self.category.filter(status = True)
    
    def thumbnail_tag(self):
        return format_html("<img width=50 height=50 style= 'border-radius:50px' src = '{}'>".format(self.thumbnail.url))
    thumbnail_tag.short_description="لوگو "

    def category_to_str(self):
        return " , ".join([category.title for category in self.category_published()])
    category_to_str.short_description=" دسته بندی"
    
    objects = FlightManager()
    
    
    

