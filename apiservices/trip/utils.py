import requests
from .models import Hadis
from requests.exceptions import HTTPError

# def fetchData():
#     url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'
#     try:
#         response = requests.get(url)
#         response.raise_for_status()
#         Trip.objects.create(
#                 title = response.json()["title"],
#                 copyright = response.json()["title"],
#                 slug = response.json()["service_version"],
#                 date = response.json()["date"],
#                 explanation = response.json()["explanation"],
#                 thumbnail = response.json()["hdurl"],
#                 service_version = response.json()["service_version"],
#             )
#         print("*****==>>> trip cronJob run")
#     except HTTPError as http_error:
#         print(f'Http error {http_error}')
#     except Exception as error:
#         print(f'Other Error { error }')
        

def hadisData():
    url = 'https://api.codebazan.ir/hadis/'
    try:
        response = requests.get(url)
        response.raise_for_status()
        Hadis.objects.create(
            sentence = response.text,
        )
        print("*****==>>> hadis cronJob run")
    except HTTPError as http_error:
        print(f'Http error {http_error}')
    except Exception as error:
        print(f'Other Error { error }')
    


