from django.contrib import admin
from .models import Flight , Hadis,Category
from django.contrib import messages
from django.utils.translation import ngettext


# change title or header dite name 
admin.site.site_title = "پنل مدیریت"
admin.site.subtitle = "پنل مدیریت"
#  make action for change the value trip
@admin.action(description="حالت پرواز")
def make_published(modeladmin, request, queryset):
    updated = queryset.update(status="p")
    modeladmin.message_user(
        request,
        ngettext(
            "%d سفر به حالت پرواز در آمد",
            "%d سفرها به حالت پرواز در آمدند",
            updated,
        )
        % updated,
        messages.SUCCESS,
    )

@admin.action(description="حالت کنسل")
def make_cancled(modeladmin, request, queryset):
    updated = queryset.update(status="d")
    modeladmin.message_user(
        request,
        ngettext(
            "%d سفر به حالت کنسل در آمد",
            "%d سفرها به حالت کنسل در آمدند",
            updated,
        )
        % updated,
        messages.ERROR,
    )
    
# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position','title','slug','status')
    list_filter = (['status'])
    search_fields = ('title','slug')
    prepopulated_fields = {'slug':('title',)}

admin.site.register(Category,CategoryAdmin)

class FlightAdmin(admin.ModelAdmin):
    list_display = ('source','destination','author','company','thumbnail_tag','slug','category_to_str',
                    'jdate','fly_time','landing_time','price','terminal')
    list_filter = ('service_type','author','source','destination','company','slug',
                    'date')
    search_fields = ('source','destination','company','slug',
                    'date','fly_time','landing_time','price','terminal')
    ordering = ["updated"]
    actions = [make_published,make_cancled]
    
    #prepopulated_fields = {"slug": ("title",)}
    def category_to_str(self,obj):
        return " , ".join([category.title for category in obj.category_published()])
    category_to_str.short_description=" دسته بندی"
    
    
admin.site.register(Flight,FlightAdmin)

class HadisAdmin(admin.ModelAdmin):
    list_display = ('sentence','jcreated')


admin.site.register(Hadis,HadisAdmin)
 

    


