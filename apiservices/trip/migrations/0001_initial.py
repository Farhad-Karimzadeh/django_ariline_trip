# Generated by Django 5.0.3 on 2024-03-20 22:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Trip',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('copyright', models.CharField(max_length=50)),
                ('slug', models.SlugField(max_length=200, unique=True)),
                ('explanation', models.TextField()),
                ('date', models.DateTimeField()),
                ('thumbnail', models.ImageField(upload_to='image')),
                ('service_version', models.CharField(max_length=10)),
            ],
        ),
    ]
