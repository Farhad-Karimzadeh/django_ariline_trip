from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_author = models.BooleanField( default=False ,verbose_name="وضعیت نویسندگی")
    spatial_user = models.DateTimeField(default = timezone.now,verbose_name="نویسنده ویژه تا ")

    def is_spatial_user(self):
        if self.spatial_user > timezone.now():
            return True
        else:
            return False
    is_spatial_user.boolean = True
    is_spatial_user.short_description = "وضعیت کاربر ویژه"

