
from django.http import Http404
from django.shortcuts import get_object_or_404
from trip.models import Flight


class FieldsMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            self.fields = ["flight_type","company","author","slug","category",
                           "source","destination","date","fly_time","landing_time",
                           "price","terminal","cargo","airplane_model","service_type",
                           "status","thumbnail"]
        elif request.user.is_author:
             self.fields = ["flight_type","company","author",
                            "slug","category","source","destination","date","fly_time",
                            "landing_time","price","status",
                            "thumbnail","service_type",
                            ]
        else:
            raise Http404("Sorry! You can't this page...")

        return super().dispatch(request, *args, **kwargs)

class FormValidMixin():
    def form_valid(self,form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit= False)
            self.obj.author = self.request.user
            self.obj.status = 'd'
        return super().form_valid(form)
    
class AuthorAccessMixin():
    def dispatch(self, request,pk, *args, **kwargs):
        flight = get_object_or_404(Flight,pk=pk)
        if  flight.author == request.user and flight.status == 'd' or \
              request.user.is_superuser :
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("Sorry! You can't edit this page...")

class SuperUserAccessMixin():
    def dispatch(self, request, *args, **kwargs):
        if   request.user.is_superuser :
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("Sorry! You can't edit this page...")



