from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    CreateView, 
    UpdateView, 
    DeleteView,
)
from trip.models import Flight
from .mixin import (
    FieldsMixin,
    FormValidMixin,
    AuthorAccessMixin,
    SuperUserAccessMixin
)

# Create your views here.

class FlightList(LoginRequiredMixin,ListView):
    # queryset = Flight.objects.all()
    template_name = "registration/home.html"
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Flight.objects.all()
        else:
            return Flight.objects.filter(author = self.request.user)

class FlightCreateView(LoginRequiredMixin,FormValidMixin,FieldsMixin,CreateView):
    model = Flight
#     fields = ["flight_type","company","author","slug","category","source","destination","date","fly_time","landing_time","price","terminal","cargo",
# "airplane_model","service_type","status","thumbnail"]
    template_name = "registration/flight_create_update.html"

class FlightUpdateView(AuthorAccessMixin,FormValidMixin,FieldsMixin,UpdateView):
    model = Flight
#     fields = ["flight_type","company","author","slug","category","source","destination","date","fly_time","landing_time","price","terminal","cargo",
# "airplane_model","service_type","status","thumbnail"]
    template_name = "registration/flight_create_update.html"

class FlightDeleteView(SuperUserAccessMixin,DeleteView):
    model = Flight
    success_url = reverse_lazy("account:home")
    template_name = "registration/flight_confirm_delete.html"
